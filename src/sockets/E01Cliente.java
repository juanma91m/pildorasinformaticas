package sockets;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class E01Cliente extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	JTextField tfMensaje;
	JButton btEnviar;
	JPanel panel;

	public E01Cliente(JFrame f_ppal) {
		super(f_ppal, ModalityType.MODELESS);
		setTitle("Video 190 y 191- Sockets (Cliente)");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);

		pack();
		setSize(500,getHeight());

		setResizable(false);
		setLocation(100,200);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.NONE;
		gbc.insets = new Insets(15, 15, 15, 15);
		
		JLabel label = new JLabel("CLIENTE");
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(label,gbc);
		
		tfMensaje = new JTextField();
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(tfMensaje,gbc);
		
		btEnviar = new JButton("Enviar");
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(btEnviar,gbc);
		
		btEnviar.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

			try {
				Socket conector = new Socket("localhost",9179);
				DataOutputStream salida = new DataOutputStream(conector.getOutputStream());
				
				salida.writeUTF(tfMensaje.getText());
				salida.close();
				conector.close();
			} catch (IOException er) {
				System.err.println(er.getMessage());
			}

	}
}