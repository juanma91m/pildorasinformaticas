package sockets;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class E01Servidor extends JDialog implements Runnable {

	private static final long serialVersionUID = 1L;
	JPanel panel;
	JTextArea taReceptor;
	ServerSocket oyente;

	public E01Servidor(JFrame f_ppal) {
		super(f_ppal, ModalityType.MODELESS);
		setTitle("Video 190 y 191- Sockets (Servidor)");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);

		pack();
		setSize(500, 500);

		setResizable(false);
		setLocation(700, 200);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(10, 10, 10, 10);

		taReceptor = new JTextArea();
		JScrollPane scroll = new JScrollPane(taReceptor);
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(scroll, gbc);

		Thread oyenteBg = new Thread(this);
		oyenteBg.start();
	}

	@Override
	public void run() {
		try {
			oyente = new ServerSocket(9179);

			while (true) {
				Socket receptor = oyente.accept();

				DataInputStream entrada = new DataInputStream(receptor.getInputStream());

				String mensaje = entrada.readUTF();
				taReceptor.append(mensaje + "\n");

				receptor.close();
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}