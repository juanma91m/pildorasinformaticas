package swing_componentes;

import java.awt.*;
import javax.swing.*;


@SuppressWarnings("serial")
public class E10JPopupMenu extends JDialog {

	private JPanel panel;
	private JPopupMenu menuEmergente;

	public E10JPopupMenu(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 109 - Menús emergentes -Click derecho-.");
		setBackground(SystemColor.window);

		panel = new JPanel();
		crearMenuEmergente();
		add(panel);

		setSize(300, 150);

		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void crearMenuEmergente() {
		menuEmergente = new JPopupMenu();
		crearMenu("Menu 1");
		crearSubMenu((JMenu) menuEmergente.getComponent(0),"Opcion 1");
		crearSubMenu((JMenu) menuEmergente.getComponent(0),"Opcion 2");
		crearSubMenu((JMenu) menuEmergente.getComponent(0),"Opcion 3");
		
		crearMenu("Menu 2");
		crearSubMenu((JMenu) menuEmergente.getComponent(1),"Opcion 1");
		crearSubMenu((JMenu) menuEmergente.getComponent(1),"Opcion 2");
		menuEmergente.addSeparator();
		crearSubMenu(null,"Aplicar");
		crearSubMenu(null,"Hacer");
		panel.setComponentPopupMenu(menuEmergente);
	}

	private void crearMenu(String titulo) {
		JMenu menu = new JMenu(titulo);
		menuEmergente.add(menu);
	}

	private void crearSubMenu(JMenu padre, String titulo) {
		JMenuItem item = new JMenuItem(titulo);
		if (padre != null)
			padre.add(item);
		else
			menuEmergente.add(item);
	}
}
