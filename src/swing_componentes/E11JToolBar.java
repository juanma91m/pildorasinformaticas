package swing_componentes;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;


@SuppressWarnings("serial")
public class E11JToolBar extends JDialog {

	private JPanel panel;
	private JToolBar barraHerramientas;
	private JButton btRojo, btVerde, btAzul,btSalir;

	public E11JToolBar(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 111 - Barra de Herramientas");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);

		setSize(300, 150);

		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void cargarComponentes() {
		panel.setLayout(new BorderLayout());
		/*panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		

		gbc.gridx = 0;
		gbc.gridy = 0;
*/
		barraHerramientas = new JToolBar();
		ImageIcon iconoRojo = new ImageIcon(new ImageIcon("src/graficos/rojo2.png").getImage().getScaledInstance(15, -1, Image.SCALE_DEFAULT));
		ImageIcon iconoVerde = new ImageIcon(new ImageIcon("src/graficos/verde2.png").getImage().getScaledInstance(15, -1, Image.SCALE_DEFAULT));
		ImageIcon iconoAzul = new ImageIcon(new ImageIcon("src/graficos/azul2.png").getImage().getScaledInstance(15, -1, Image.SCALE_DEFAULT));
		ImageIcon iconoSalir = new ImageIcon(new ImageIcon("src/graficos/salir.png").getImage().getScaledInstance(15, -1, Image.SCALE_DEFAULT));
		Action accionSalir = new AbstractAction(null,iconoSalir) {
			
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
			
		};
		
		
		btRojo = new JButton(new AccionColor("Rojo",iconoRojo,Color.RED));
		btVerde = new JButton(new AccionColor("Amarillo",iconoVerde,Color.GREEN));
		btAzul = new JButton(new AccionColor("Azul",iconoAzul,Color.BLUE));
		btSalir = new JButton(accionSalir);
		barraHerramientas.add(btRojo);
		barraHerramientas.add(btVerde);
		barraHerramientas.add(btAzul);
		barraHerramientas.addSeparator();
		barraHerramientas.add(btSalir);
		panel.add(barraHerramientas,BorderLayout.NORTH);
	}
	
	class AccionColor extends AbstractAction {
		
		public AccionColor(String nombre,Icon icono, Color color ) {
			putValue(Action.SMALL_ICON,icono);
			putValue(Action.SHORT_DESCRIPTION,"Pone el color de fondo en "+nombre);
			putValue("Color",color);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Color c = (Color) getValue("Color");
			panel.setBackground(c);
			
		}
		
	}
}
