package threads;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class E03SincronizarThreadsConSyncronized extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JLabel explicacion;
	private JButton btIniciar, btDetener;

	public E03SincronizarThreadsConSyncronized(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 178 - Sincronizacion de hilos (Threads).");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);
		pack();

		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(15, 15, 15, 15);

		explicacion = new JLabel(
				"<html><body>Este programa iniciara una simulación de movimientos<br>entre 100 cuentas bancarias</body></html>");
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(explicacion, gbc);

		btIniciar = new JButton("Iniciar programa");
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(btIniciar, gbc);

		btDetener = new JButton("Detener programa");
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(btDetener, gbc);

		btIniciar.addActionListener(this);
		btDetener.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(btIniciar)) {
			Banco b = new Banco();

			for (int i = 0; i < 100; i++) {
				EjecutaTransferencia r = new EjecutaTransferencia(b, i, 2000);
				Thread t = new Thread(r);
				t.start();
			}
		} else if (boton.equals(btDetener)) {
			for (Thread t : Thread.getAllStackTraces().keySet())
				if (t.getName().contains("Thread-") && (!t.getName().equals("Thread-0") && !t.isInterrupted()
						&& t.getState() != Thread.State.TERMINATED)) {
					t.interrupt();
					t = null;
				}
		}
	}

	class Banco {

		private final double[] cuentas;

		public Banco() {
			cuentas = new double[100];
			for (int i = 0; i < cuentas.length; i++)
				cuentas[i] = 2000;
		}

		public synchronized void transferencia(int ctaOrigen, int ctaDestino, double cantidadTransferida)
				throws InterruptedException {
			while (cuentas[ctaOrigen] < cantidadTransferida) {
				//solo se puede establecer una sola condición con este método. Con el método anterior
				//se pueden establecer tantas como querramos.
				wait();
			}
			System.out.println(Thread.currentThread());
			cuentas[ctaOrigen] -= cantidadTransferida;
			System.out.printf("%10.2f de %d para %d ", cantidadTransferida, ctaOrigen, ctaDestino);
			cuentas[ctaDestino] += cantidadTransferida;
			System.out.printf("Saldo total: %10.2f%n", getSaldoTotal());
			notifyAll();
		}

		public double getSaldoTotal() {
			double saldoTotal = 0;
			for (int i = 0; i < cuentas.length; i++)
				saldoTotal += cuentas[i];
			return saldoTotal;
		}
	}

	class EjecutaTransferencia implements Runnable {

		private Banco banco;
		private int ctaOrigen;
		private double maximoImporte;

		public EjecutaTransferencia(Banco b, int ctaOrigen, double maximoImporte) {
			this.banco = b;
			this.ctaOrigen = ctaOrigen;
			this.maximoImporte = maximoImporte;
		}

		@Override
		public void run() {

			try {
				while (true) {
					int ctaDestino = (int) (Math.random() * 100);
					double cantidad = Math.random() * maximoImporte;
					banco.transferencia(ctaOrigen, ctaDestino, cantidad);
					Thread.sleep(10);
				}
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}