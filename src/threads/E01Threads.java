package threads;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class E01Threads extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JLabel hilo1, hilo2, hilo3, hilo4;
	private JButton btHilo1, btHilo2, btHilo3, btHilo4, btResetear;
	private List<IniciarConteo> listaRunnable = new ArrayList<IniciarConteo>();
	private Thread t1 = null, t2 = null, t3 = null, t4 = null;
	private int i1, i2, i3, i4;

	public E01Threads(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 168 al 170 - Programación por hilos (Threads).");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);

		pack();

		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(15, 15, 15, 15);

		hilo1 = new JLabel("0");
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(hilo1, gbc);

		hilo2 = new JLabel("0");
		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(hilo2, gbc);

		hilo3 = new JLabel("0");
		gbc.gridx = 2;
		gbc.gridy = 0;
		panel.add(hilo3, gbc);

		hilo4 = new JLabel("0");
		gbc.gridx = 3;
		gbc.gridy = 0;
		panel.add(hilo4, gbc);

		btHilo1 = new JButton("Hilo 1");
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(btHilo1, gbc);

		btHilo2 = new JButton("Hilo 2");
		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(btHilo2, gbc);

		btHilo3 = new JButton("Hilo 3");
		gbc.gridx = 2;
		gbc.gridy = 1;
		panel.add(btHilo3, gbc);

		btHilo4 = new JButton("Hilo 4");
		gbc.gridx = 3;
		gbc.gridy = 1;
		panel.add(btHilo4, gbc);

		btResetear = new JButton("Resetear todos");
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 4;
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(btResetear, gbc);

		btHilo1.addActionListener(this);
		btHilo2.addActionListener(this);
		btHilo3.addActionListener(this);
		btHilo4.addActionListener(this);
		btResetear.addActionListener(this);

		listaRunnable.add(new IniciarConteo(hilo1, i1));
		listaRunnable.add(new IniciarConteo(hilo2, i2));
		listaRunnable.add(new IniciarConteo(hilo3, i3));
		listaRunnable.add(new IniciarConteo(hilo4, i4));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();

		if (boton.equals(btHilo1)) {
			if (t1 == null || t1.getState() == Thread.State.TERMINATED) {
				t1 = new Thread(listaRunnable.get(0));
				t1.start();
			} else if (!t1.isInterrupted()) {
				t1.interrupt();
			}
		} else if (boton.equals(btHilo2)) {
			if (t2 == null || t2.getState() == Thread.State.TERMINATED) {
				t2 = new Thread(listaRunnable.get(1));
				t2.start();
			} else if (!t2.isInterrupted()) {
				t2.interrupt();
			}
		} else if (boton.equals(btHilo3)) {
			if (t3 == null || t3.getState() == Thread.State.TERMINATED) {
				t3 = new Thread(listaRunnable.get(2));
				t3.start();
			} else if (!t3.isInterrupted()) {
				t3.interrupt();
			}
		} else if (boton.equals(btHilo4)) {
			if (t4 == null || t4.getState() == Thread.State.TERMINATED) {
				t4 = new Thread(listaRunnable.get(3));
				t4.start();
			} else if (!t4.isInterrupted()) {
				t4.interrupt();
			}
		} else if (boton.equals(btResetear)) {
			i1 = 0;
			i2 = 0;
			i3 = 0;
			i4 = 0;
			hilo1.setText("0");
			hilo2.setText("0");
			hilo3.setText("0");
			hilo4.setText("0");

			//finalizar todos los Threads a excepcion de thread-0 que es el hilo del programa
			//y los threads propios de java
			for (Thread t : Thread.getAllStackTraces().keySet())
				if (t.getName().contains("Thread-") && (!t.getName().equals("Thread-0") && !t.isInterrupted()
						&& t.getState() != Thread.State.TERMINATED)) {
					t.interrupt();
					t = null;
				}

			hilo1.setText("0");
			hilo2.setText("0");
			hilo3.setText("0");
			hilo4.setText("0");

			listaRunnable = new ArrayList<IniciarConteo>();
			listaRunnable.add(0, new IniciarConteo(hilo1, i1));
			listaRunnable.add(1, new IniciarConteo(hilo2, i2));
			listaRunnable.add(2, new IniciarConteo(hilo3, i3));
			listaRunnable.add(3, new IniciarConteo(hilo4, i4));
		}
	}

	class IniciarConteo implements Runnable {

		private JLabel label;
		private int i;

		public IniciarConteo(JLabel label, int i) {
			this.label = label;
			this.i = i;
		}

		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				label.setText(Integer.toString(i));
				i++;
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}
	}
}