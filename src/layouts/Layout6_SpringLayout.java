package layouts;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class Layout6_SpringLayout extends JDialog implements ActionListener {

	JPanel miLamina;
	JLabel lbUsuario, lbContrasena;
	JTextField tfUsuario;
	JPasswordField pfContrasena;
	JButton btOk, btCancel;

	public Layout6_SpringLayout(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 116 - Layout SpringLayout");
		setBackground(SystemColor.window);
		setSize(250, 250);
		setLocationRelativeTo(f_ppal);

		miLamina = new JPanel();
		cargarComponentes();
		add(miLamina);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	public void cargarComponentes() {
		miLamina.setLayout(new BorderLayout());
		lbUsuario = new JLabel("Usuario");
		tfUsuario = new JTextField(10);
		tfUsuario.setMaximumSize(tfUsuario.getPreferredSize());

		Box cajaUsuario = Box.createHorizontalBox();
		cajaUsuario.add(lbUsuario);
		cajaUsuario.add(Box.createHorizontalStrut(10));
		cajaUsuario.add(tfUsuario);

		lbContrasena = new JLabel("Contraseña");
		pfContrasena = new JPasswordField(10);
		pfContrasena.setMaximumSize(pfContrasena.getPreferredSize());

		Box cajaContrasena = Box.createHorizontalBox();
		cajaContrasena.add(lbContrasena);
		cajaContrasena.add(Box.createHorizontalStrut(10));
		cajaContrasena.add(pfContrasena);

		btOk = new JButton("Confirmar");
		btCancel = new JButton("Cancelar");

		Box cajaBotones = Box.createHorizontalBox();
		cajaBotones.add(btOk);
		cajaBotones.add(Box.createGlue());
		cajaBotones.add(btCancel);

		Box cajaVertical = Box.createVerticalBox();
		cajaVertical.add(cajaUsuario);
		cajaVertical.add(Box.createVerticalStrut(10));
		cajaVertical.add(cajaContrasena);
		cajaVertical.add(Box.createVerticalStrut(10));
		cajaVertical.add(cajaBotones);
		miLamina.add(cajaVertical,BorderLayout.CENTER);

		btOk.addActionListener(this);
		btCancel.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(btCancel))
			dispose();
	}
}