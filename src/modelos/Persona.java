package modelos;

import java.io.Serializable;

public class Persona implements Serializable{
	//esta clase es usada en el video 157 donde se vela serializacion. Solapa Archivos
	// tambien es usada en el video 158 donde se explica el versionado de la huella serialVersionUID
	
	private static final long serialVersionUID=1L;
	
	private static int count = 0;
	private int id;
	private String nombre;
	private String apellido;
	private int edad;

	public Persona(String nombre, String apellido, int edad) {
		this.id 		= ++count;
		this.nombre		= nombre;
		this.apellido	= apellido;
		this.edad 		= edad;
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public int getEdad() {
		return edad;
	}

	@Override
	public String toString() {
		return "Persona [id="+id+", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + "]";
	}
}
