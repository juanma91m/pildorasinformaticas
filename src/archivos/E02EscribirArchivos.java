package archivos;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;


public class E02EscribirArchivos extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JButton btEscribirFileWriter,btEscribirBufferedWriter;
	private JTextArea taArchivo;
	
	public E02EscribirArchivos(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 153 y 154 - escribir un archivo + buffer");
		panel = new JPanel();
		cargarComponentes();
		add(panel);
		setSize(500,400);
		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true); // Set visible siempre a lo último en los JDialog. Por alguna razón no carga
							// componentes si la hacemos antes.
	}
	
	public void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		taArchivo = new JTextArea();
		JScrollPane scroll = new JScrollPane(taArchivo);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridheight = 2;
		gbc.insets = new Insets(15, 15, 15, 0);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		panel.add(scroll,gbc);
		gbc.gridheight = 1;
		
		btEscribirFileWriter = new JButton("Escribir con FileWriter");
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(15, 15, 15, 15);
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		panel.add(btEscribirFileWriter,gbc);
		
		btEscribirBufferedWriter = new JButton("Escribir con BufferedWriter");
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.NONE;
		gbc.insets = new Insets(15, 15, 15, 15);
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		panel.add(btEscribirBufferedWriter,gbc);
		
		btEscribirFileWriter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				escribirArchivoFileWriter();		
			}
		});
		
		btEscribirBufferedWriter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				escribirArchivoBufferedWriter();		
			}
		});	
	}
	
	public void escribirArchivoFileWriter() {
		try {
			FileWriter salida = new FileWriter("/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos/prueba escritura.txt");
			salida.write(taArchivo.getText());

			salida.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void escribirArchivoBufferedWriter() {
		try {
			FileWriter salida = new FileWriter("/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos/prueba escritura.txt");
			BufferedWriter bufferEscritura = new BufferedWriter(salida);
			
			bufferEscritura.write(taArchivo.getText());

			bufferEscritura.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}