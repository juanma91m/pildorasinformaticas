package archivos;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

import admin.SwingUtils;

public class E03LeerEscribirBytes extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTextArea taConsola;
	private JButton btLeerFileInputStream, btEscribirFileOutputStream;
	private List<Integer> listaBytes;
	private JFileChooser fcAbrirArchivo, fcSalvarArchivo;

	public E03LeerEscribirBytes(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 155 y 156 - leer un archivo");
		panel = new JPanel();
		cargarComponentes();
		add(panel);
		setSize(1100, 700);
		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true); // Set visible siempre a lo último en los JDialog. Por alguna razón no carga
							// componentes si la hacemos antes.
	}

	public void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		taConsola = new JTextArea();
		JScrollPane scroll = new JScrollPane(taConsola);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridheight = 4;
		gbc.insets = new Insets(15, 15, 15, 0);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		panel.add(scroll, gbc);
		gbc.gridheight = 1;

		btLeerFileInputStream = new JButton("Leer FileInputStream", new ImageIcon("src/graficos/Open16.gif"));
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(15, 15, 15, 15);
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		panel.add(btLeerFileInputStream, gbc);

		btEscribirFileOutputStream = new JButton("Escribir FileOutputStream", new ImageIcon("src/graficos/Save16.gif"));
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.insets = new Insets(15, 15, 15, 15);
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		panel.add(btEscribirFileOutputStream, gbc);

		btLeerFileInputStream.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fcAbrirArchivo = new JFileChooser(
						"/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos");

				// Ver vista detalle en JFileChooser - ver en paquete admin la clase
				// SwingUtils.java
				SwingUtils.getDescendantOfType(AbstractButton.class, fcAbrirArchivo, "Icon",
						UIManager.getIcon("FileChooser.detailsViewIcon")).doClick();

				int out = fcAbrirArchivo.showOpenDialog(getParent());

				if (out == JFileChooser.APPROVE_OPTION && fcAbrirArchivo.getSelectedFile() != null)
					leerBytesFileInputStream(fcAbrirArchivo.getSelectedFile());
				else
					System.err.println("No seleccionó ningún archivo.");
			}
		});

		btEscribirFileOutputStream.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fcSalvarArchivo = new JFileChooser(
						"/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos");
				int out = fcSalvarArchivo.showSaveDialog(getParent());

				if (out == JFileChooser.APPROVE_OPTION && fcSalvarArchivo.getSelectedFile() != null)
					EscribirBytesFileOutputStream(fcSalvarArchivo.getSelectedFile());
				else
					System.err.println("No seleccionó ningún archivo.");
			}
		});

	}

	public void leerBytesFileInputStream(File file) {
		try {
			taConsola.setText("Estos son los bytes de la imagen:");
			FileInputStream entrada = new FileInputStream(file);
			listaBytes = new ArrayList<Integer>();
			boolean finalArchivo = false;
			Integer byteEntrada;
			while (!finalArchivo) {
				byteEntrada = entrada.read();
				if (byteEntrada == -1)
					finalArchivo = true;
				else {
					taConsola.append("\n" + byteEntrada.toString());
					listaBytes.add(byteEntrada);
				}
			}
			taConsola.setCaretPosition(0);
			entrada.close();
		} catch (IOException e) {
			System.err.println("Fichero no encontrado");
		}
	}

	public void EscribirBytesFileOutputStream(File file) {

		try {
			FileOutputStream salida = new FileOutputStream(file);
			for (int i = 0; i < listaBytes.size(); i++)
				salida.write(listaBytes.get(i));

			salida.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}