package archivos;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.*;
import admin.SwingUtils;
import modelos.Persona;

public class E04Serializacion extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JButton btEntradaFichero, btSalidaFichero;
	private JFileChooser fcEntradaFichero, fcSalidaFichero;
	private JList<Persona> listaPersonas;

	public E04Serializacion(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 157, 158 - Serializacion y serialVersionUID");
		panel = new JPanel();
		cargarComponentes();
		add(panel);
		pack();
		setSize(750, 300);
		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true); // Set visible siempre a lo último en los JDialog. Por alguna razón no carga
							// componentes si la hacemos antes.
	}

	public void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		listaPersonas = new JList<Persona>();
		JScrollPane scroll = new JScrollPane(listaPersonas);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(15, 15, 15, 15);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 2;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		panel.add(scroll, gbc);
		gbc.gridheight = 1;
		
		btSalidaFichero = new JButton("Escribir objeto", new ImageIcon("src/graficos/Save16.gif"));
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		gbc.insets = new Insets(15, 0, 15, 15);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		panel.add(btSalidaFichero, gbc);

		btEntradaFichero = new JButton("Leer objeto", new ImageIcon("src/graficos/Open16.gif"));
		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(btEntradaFichero, gbc);



		btSalidaFichero.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fcSalidaFichero = new JFileChooser(
						"/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos");
				int out = fcSalidaFichero.showSaveDialog(getParent());

				if (out == JFileChooser.APPROVE_OPTION && fcSalidaFichero.getSelectedFile() != null)
					EscribirObjetoEnFichero(fcSalidaFichero.getSelectedFile());
				else
					System.err.println("No seleccionó ningún archivo.");
			}
		});

		btEntradaFichero.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fcEntradaFichero = new JFileChooser(
						"/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos");

				// Ver vista detalle en JFileChooser - ver en paquete admin la clase
				// SwingUtils.java
				SwingUtils.getDescendantOfType(AbstractButton.class, fcEntradaFichero, "Icon",
						UIManager.getIcon("FileChooser.detailsViewIcon")).doClick();

				int out = fcEntradaFichero.showOpenDialog(getParent());

				if (out == JFileChooser.APPROVE_OPTION && fcEntradaFichero.getSelectedFile() != null)
					LeerObjetoEnFichero(fcEntradaFichero.getSelectedFile());
				else
					System.err.println("No seleccionó ningún archivo.");
			}
		});
	}

	private void EscribirObjetoEnFichero(File file) {
		Persona persona1 = new Persona("Juan Manuel", "Magnaterra", 27);
		Persona persona2 = new Persona("Lorena", "Presa", 39);
		DefaultListModel<Persona> personas = new DefaultListModel<Persona>();
		personas.addElement(persona1);
		personas.addElement(persona2);
		try {
			ObjectOutputStream salidaObjeto = new ObjectOutputStream(new FileOutputStream(file));
			salidaObjeto.writeObject(personas);
			salidaObjeto.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void LeerObjetoEnFichero(File file) {
		try {
			ObjectInputStream entradaObjeto = new ObjectInputStream(new FileInputStream(file));
			DefaultListModel<Persona> personasRecuperadas = new DefaultListModel<Persona>();
			personasRecuperadas = (DefaultListModel<Persona>) entradaObjeto.readObject();
			entradaObjeto.close();
	
			listaPersonas.setModel(personasRecuperadas);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}