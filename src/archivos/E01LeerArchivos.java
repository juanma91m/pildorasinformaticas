package archivos;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class E01LeerArchivos extends JDialog{
	
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTextArea taConsola;
	private JButton btLeerFileReader, btLeerBufferedReader;
	
	public E01LeerArchivos(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 152, 153 y 154 - leer un archivo + buffer");
		panel = new JPanel();
		cargarComponentes();
		add(panel);
		setSize(400,400);
		setLocationRelativeTo(f_ppal);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true); // Set visible siempre a lo último en los JDialog. Por alguna razón no carga
							// componentes si la hacemos antes.
	}
	
	public void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		taConsola = new JTextArea();
		JScrollPane scroll = new JScrollPane(taConsola);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridheight = 2;
		gbc.insets = new Insets(15, 15, 15, 0);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		panel.add(scroll,gbc);
		gbc.gridheight = 1;
		
		btLeerFileReader = new JButton("Leer con FileReader");
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(15, 15, 15, 15);
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		panel.add(btLeerFileReader,gbc);
		
		btLeerBufferedReader = new JButton("Leer con BufferedReader");
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.NONE;
		gbc.insets = new Insets(15, 15, 15, 15);
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		panel.add(btLeerBufferedReader,gbc);
		
		btLeerFileReader.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				leerArchivoFileReader();		
			}
		});	
		
		btLeerBufferedReader.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				leerArchivoBufferedReader();		
			}
		});	
		
	}
	
	public void leerArchivoFileReader() {
		try {
			taConsola.setText("");
			FileReader entrada = new FileReader("/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos/prueba lectura.txt");
			int c = entrada.read();
			while (c != -1) {	
				char caracterLeido = (char) c;
				taConsola.append(String.valueOf(caracterLeido));
				c = entrada.read();
			}
			entrada.close();
		} catch (IOException e) {
			System.err.println("Fichero no encontrado");
		}
		
		//eliminar ultima linea
		if (taConsola.getText().endsWith("\n")) { 
		    Document doc = taConsola.getDocument(); 
		    try {
				doc.remove(doc.getLength() - 1, 1);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void leerArchivoBufferedReader() {
		taConsola.setText("");
		try {
			FileReader entrada = new FileReader("/home/jmmagnaterra/eclipse-workspace/pildorasinformaticas/src/archivos/prueba lectura.txt");
			BufferedReader bufferLectura = new BufferedReader(entrada);
			String linea = bufferLectura.readLine();
			
			while (linea!=null) {
				taConsola.append(linea+"\n");
				linea = bufferLectura.readLine();
			}
			bufferLectura.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//eliminar ultima linea
		if (taConsola.getText().endsWith("\n")) { 
		    Document doc = taConsola.getDocument(); 
		    try {
				doc.remove(doc.getLength() - 1, 1);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
	}
}