package archivos;

import java.io.File;
import java.io.IOException;

public class E05File {

	public E05File() {
		// fichero no existente
		File file1 = new File("ejemplo archivos (clase File) - Video 159 y 160");
		System.out.println("file1.getAbsolutePath(): " + file1.getAbsolutePath());
		System.out.println("file1.exists(): " + file1.exists());
		System.out.println();

		// fichero existente
		File file2 = new File("bin");
		System.out.println("file2.getAbsolutePath(): " + file2.getAbsolutePath());
		System.out.println("file2.exists(): " + file2.exists());
		System.out.println();

		// que hay dentro de una carpeta y sus subcarpetas. Para asegurarnos que el
		// separador de carpetas
		// sea el correcto del SO usar la siguiente sentencia
		File ruta = new File("src/archivos".replace("/", File.separator));
		System.out.println("En la carpeta " + ruta.getAbsolutePath() + " hay:");
		String listaArchivos[] = ruta.list();
		for (int i = 0; i < listaArchivos.length; i++) {
			System.out.println(listaArchivos[i]);
			File f = new File(ruta.getAbsolutePath(), listaArchivos[i]);
			if (f.isDirectory()) {
				String listaArchivosSubCarpeta[] = f.list();
				for (int e = 0; e < listaArchivosSubCarpeta.length; e++)
					System.out.println("  " + listaArchivosSubCarpeta[e]);
			}
		}
		System.out.println();

		// crear directorio
		File ruta2 = new File("src/archivos/directorio_creado_por_programa".replace("/", File.separator));
		if (!ruta2.exists())
			ruta2.mkdir();

		// crear archivo, no hace falta preguntar si existe el archivo ya que el método si existe no lo reemplaza
		File file3 = new File((ruta2.getAbsolutePath() + "/archivo_creado_por_programa").replace("/", File.separator));
		try {
			file3.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//eliminamos el archivo recién creado
		file3.delete();
	}
}
