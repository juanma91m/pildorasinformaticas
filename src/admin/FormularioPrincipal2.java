package admin;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.*;
import archivos.*;
import eventos.*;
import graficos.*;
import layouts.*;
import practicas.*;
import practicas.chat.E03ChatCliente;
import practicas.chat.E03ChatServidor;
import sockets.E01Cliente;
import sockets.E01Servidor;
import swing_componentes.*;
import threads.E01Threads;
import threads.E02SincronizarThreads;
import threads.E03SincronizarThreadsConSyncronized;

public class FormularioPrincipal2 extends JFrame implements ActionListener, WindowListener {

	private static final long serialVersionUID = 1L;

	private JMenuBar menuBar;
	private JMenu graficos, eventos, layouts, swing, practicas, archivos, threads, sockets, ayuda;
	private JMenuItem btImageIO, btGraphics2D,

			btEventosBG, btEventosBG2, btEventosVentana, btEventosEstadoVentana, btEventosTeclado, btEventosMouse,
			btEventosFoco, btEventosFocoVentana, btMultiplesFuentes,

			btFlowlayout, btBorderlayout, btMultiplesLayouts, btGridlayout_calculadora, btBox, btSpringLayout,

			btTF, btEventosTF, btJPasswordField, btTA, btCheckRadio, btJComboBox, btJSlider, btJSpinner, btJMenu,
			btJPopupMenu, btJToolBar,

			btProcesadorTexto, btProcesadorTexto2, btChatCliente, btChatServidor,

			btLeerArchivo, btEscribirArchivo, btLeerEscribirBytes, btSerializacion, btFile,

			btThreads, btSincronizacionThreads, btSincronizacionThreadsSincronized,

			btCliente, btServidor,

			btSalir;

	public FormularioPrincipal2() {
		setTitle("App organizadora de prácticas de píldoras informáticas");

		// Ajustar tamaño ventana
		Toolkit pantalla = Toolkit.getDefaultToolkit();
		int altoPantalla = pantalla.getScreenSize().height;
		int anchoPantalla = pantalla.getScreenSize().width;
		setSize(anchoPantalla, altoPantalla - 250);

		cargarMenu();

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void cargarMenu() {
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridheight = 1;// alto
		gbc.gridwidth = 1;// ancho
		gbc.weightx = 1.0; // que no se estiren horizontalmente
		gbc.weighty = 1.0;// que se estiren verticalmente
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;

		menuBar = new JMenuBar();

		graficos = new JMenu("Formas y figuras (Graphics2D)");
		menuBar.add(graficos);
		cargarComponentesGraficos();

		eventos = new JMenu("Eventos");
		menuBar.add(eventos);
		cargarComponentesEventos();

		layouts = new JMenu("Layouts (Swing)");
		menuBar.add(layouts);
		cargarComponentesLayout();

		swing = new JMenu("Componentes Swing");
		menuBar.add(swing);
		cargarComponentesSwing();

		practicas = new JMenu("Prácticas");
		menuBar.add(practicas);
		cargarComponentesPractica();

		archivos = new JMenu("Archivos");
		menuBar.add(archivos);
		cargarComponentesArchivos();

		threads = new JMenu("Threads (hilos)");
		menuBar.add(threads);
		cargarComponentesThreads();

		sockets = new JMenu("Sockets");
		menuBar.add(sockets);
		cargarComponentesSockets();

		ayuda = new JMenu("Ayuda");
		menuBar.add(ayuda);
		cargarComponentesAyuda();

		getContentPane().add(menuBar, gbc);
	}

	private void cargarComponentesGraficos() {
		btImageIO = new JMenuItem("Clase ImageIO");
		graficos.add(btImageIO);

		btGraphics2D = new JMenuItem("Clase Graphics2D");
		graficos.add(btGraphics2D);

		btImageIO.addActionListener(this);
		btGraphics2D.addActionListener(this);
	}

	private void cargarComponentesLayout() {
		btFlowlayout = new JMenuItem("FlowLayout");
		layouts.add(btFlowlayout);

		btBorderlayout = new JMenuItem("BorderLayout");
		layouts.add(btBorderlayout);

		btMultiplesLayouts = new JMenuItem("Múltiples Layouts");
		layouts.add(btMultiplesLayouts);

		btGridlayout_calculadora = new JMenuItem("GridLayout Calculadora");
		layouts.add(btGridlayout_calculadora);

		btBox = new JMenuItem("Box");
		layouts.add(btBox);

		btSpringLayout = new JMenuItem("SpringLayout");
		layouts.add(btSpringLayout);

		btFlowlayout.addActionListener(this);
		btBorderlayout.addActionListener(this);
		btMultiplesLayouts.addActionListener(this);
		btGridlayout_calculadora.addActionListener(this);
		btBox.addActionListener(this);
		btSpringLayout.addActionListener(this);
	}

	private void cargarComponentesEventos() {
		// primera opcion
		JMenu eventosGeneral = new JMenu("Generales");
		eventos.add(eventosGeneral);
		btEventosBG = new JMenuItem("JButton");
		eventosGeneral.add(btEventosBG);
		btEventosBG2 = new JMenuItem("idem c/clase oyente");
		eventosGeneral.add(btEventosBG2);

		// segunda opcion
		JMenu eventosVentanaEstado = new JMenu("Ventana y cambio de estado");
		eventos.add(eventosVentanaEstado);
		btEventosVentana = new JMenuItem("Ventana");
		eventosVentanaEstado.add(btEventosVentana);
		btEventosEstadoVentana = new JMenuItem("Cambio estado ventana");
		eventosVentanaEstado.add(btEventosEstadoVentana);

		// tercera opcion
		JMenu eventosTecladoMouse = new JMenu("Teclado y mouse");
		eventos.add(eventosTecladoMouse);
		btEventosTeclado = new JMenuItem("De teclado");
		eventosTecladoMouse.add(btEventosTeclado);
		btEventosMouse = new JMenuItem("De mouse");
		eventosTecladoMouse.add(btEventosMouse);

		// cuarta opcion
		JMenu eventosFocoVentana = new JMenu("Foco y foco-ventana");
		eventos.add(eventosFocoVentana);
		btEventosFoco = new JMenuItem("De foco");
		eventosFocoVentana.add(btEventosFoco);
		btEventosFocoVentana = new JMenuItem("De foco-ventana");
		eventosFocoVentana.add(btEventosFocoVentana);

		// quinto elemento
		btMultiplesFuentes = new JMenuItem("Múltiples fuentes");
		eventos.add(btMultiplesFuentes);

		btEventosBG.addActionListener(this);
		btEventosBG2.addActionListener(this);
		btEventosVentana.addActionListener(this);
		btEventosEstadoVentana.addActionListener(this);
		btEventosTeclado.addActionListener(this);
		btEventosMouse.addActionListener(this);
		btEventosFoco.addActionListener(this);
		btEventosFocoVentana.addActionListener(this);
		btMultiplesFuentes.addActionListener(this);
	}

	private void cargarComponentesSwing() {
		btTF = new JMenuItem("JTextField");
		swing.add(btTF);

		btEventosTF = new JMenuItem("eventos JTextField");
		swing.add(btEventosTF);

		btJPasswordField = new JMenuItem("JPasswordField");
		swing.add(btJPasswordField);

		btTA = new JMenuItem("JTextArea");
		swing.add(btTA);

		btCheckRadio = new JMenuItem("JCheckBox y JRadioButton");
		swing.add(btCheckRadio);

		btJComboBox = new JMenuItem("JComboBox");
		swing.add(btJComboBox);

		btJSlider = new JMenuItem("JSlider");
		swing.add(btJSlider);

		btJSpinner = new JMenuItem("JSpinner");
		swing.add(btJSpinner);

		btJMenu = new JMenuItem("JMenu");
		swing.add(btJMenu);

		btJPopupMenu = new JMenuItem("JPopupMenu");
		swing.add(btJPopupMenu);

		btJToolBar = new JMenuItem("JToolBar");
		swing.add(btJToolBar);

		btTF.addActionListener(this);
		btEventosTF.addActionListener(this);
		btJPasswordField.addActionListener(this);
		btTA.addActionListener(this);
		btCheckRadio.addActionListener(this);
		btJComboBox.addActionListener(this);
		btJSlider.addActionListener(this);
		btJSpinner.addActionListener(this);
		btJMenu.addActionListener(this);
		btJPopupMenu.addActionListener(this);
		btJToolBar.addActionListener(this);
	}

	private void cargarComponentesPractica() {
		btProcesadorTexto = new JMenuItem("Procesador de texto");
		practicas.add(btProcesadorTexto);

		btProcesadorTexto2 = new JMenuItem("Procesador de texto 2");
		practicas.add(btProcesadorTexto2);

		btChatCliente = new JMenuItem("Chat Cliente");
		practicas.add(btChatCliente);

		btChatServidor = new JMenuItem("Chat Servidor");
		practicas.add(btChatServidor);

		btChatCliente.addActionListener(this);
		btChatServidor.addActionListener(this);
		btProcesadorTexto.addActionListener(this);
		btProcesadorTexto2.addActionListener(this);
	}

	private void cargarComponentesArchivos() {
		btLeerArchivo = new JMenuItem("Leer Archivos");
		archivos.add(btLeerArchivo);

		btEscribirArchivo = new JMenuItem("Escribir Archivos");
		archivos.add(btEscribirArchivo);

		btLeerEscribirBytes = new JMenuItem("FileInput(Output)Stream leer y escribir bytes y JFileChooser");
		archivos.add(btLeerEscribirBytes);

		btSerializacion = new JMenuItem("Serializacion");
		archivos.add(btSerializacion);

		btFile = new JMenuItem("Clase File");
		archivos.add(btFile);

		btLeerArchivo.addActionListener(this);
		btEscribirArchivo.addActionListener(this);
		btLeerEscribirBytes.addActionListener(this);
		btSerializacion.addActionListener(this);
		btFile.addActionListener(this);
	}

	private void cargarComponentesThreads() {
		btThreads = new JMenuItem("Hilos (Threads)");
		threads.add(btThreads);

		btSincronizacionThreads = new JMenuItem("Sincronizar hilos");
		threads.add(btSincronizacionThreads);

		btSincronizacionThreadsSincronized = new JMenuItem("Sincronizar hilos con syncronized");
		threads.add(btSincronizacionThreadsSincronized);

		btThreads.addActionListener(this);
		btSincronizacionThreads.addActionListener(this);
		btSincronizacionThreadsSincronized.addActionListener(this);
	}

	private void cargarComponentesSockets() {
		btCliente = new JMenuItem("Cliente de chat");
		sockets.add(btCliente);

		btServidor = new JMenuItem("Servidor de chat");
		sockets.add(btServidor);

		btCliente.addActionListener(this);
		btServidor.addActionListener(this);
	}

	private void cargarComponentesAyuda() {
		btSalir = new JMenuItem("Salir");
		ayuda.add(btSalir);

		btSalir.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem boton = (JMenuItem) e.getSource();
		if (boton.equals(btImageIO))
			new PruebaImageIO(this);
		else if (boton.equals(btGraphics2D))
			new PruebaJava2D(this);
		else if (boton.equals(btEventosBG))
			new E01Eventos1y2CambiarBackground(this);
		else if (boton.equals(btEventosBG2))
			new E02Eventos3CambiarBackgroundMejorado(this);
		else if (boton.equals(btEventosVentana))
			new E03EventosDeVentana4(this);
		else if (boton.equals(btEventosEstadoVentana))
			new E04EventosDeEstadoVentana6(this);
		else if (boton.equals(btEventosTeclado))
			new E05EventosDeTeclado7(this);
		else if (boton.equals(btEventosMouse))
			new E06EventosDeRaton8y9(this);
		else if (boton.equals(btEventosFoco))
			new E07EventosDeFoco10(this);
		else if (boton.equals(btEventosFocoVentana)) {
			E08EventosDeFocoVentana11 formulario = new E08EventosDeFocoVentana11();
			formulario.iniciar();
		} else if (boton.equals(btMultiplesFuentes))
			new E09MultiplesFuentes121314y15(this);
		else if (boton.equals(btFlowlayout))
			new Layout1_FlowLayout(this);
		else if (boton.equals(btBorderlayout))
			new Layout2_BorderLayout(this);
		else if (boton.equals(btMultiplesLayouts))
			new Layout3_MultiplesLayout(this);
		else if (boton.equals(btGridlayout_calculadora))
			new Layout4_GridLayout_Calculadora(this);
		else if (boton.equals(btBox))
			new Layout5_Box(this);
		else if (boton.equals(btSpringLayout))
			new Layout6_SpringLayout(this);
		else if (boton.equals(btTF))
			new E01CuadrosDeTexto0102(this);
		else if (boton.equals(btEventosTF))
			new E02CuadrosDeTexto03_eventosDeTexto(this);
		else if (boton.equals(btJPasswordField))
			new E03EventosDeTexto04_JPasswordField_GridbagLayout(this);
		else if (boton.equals(btTA))
			new E04AreasDeTexto05(this);
		else if (boton.equals(btCheckRadio))
			new E05JCheckBox01_JRadioButton02(this);
		else if (boton.equals(btJComboBox))
			new E06JComboBox(this);
		else if (boton.equals(btJSlider))
			new E07JSlider(this);
		else if (boton.equals(btJSpinner))
			new E08JSpinner(this);
		else if (boton.equals(btJMenu))
			new E09JMenu(this);
		else if (boton.equals(btProcesadorTexto))
			new E01ProcesadorTexto(this);
		else if (boton.equals(btProcesadorTexto2))
			new E02ProcesadorTexto2(this);
		else if (boton.equals(btJPopupMenu))
			new E10JPopupMenu(this);
		else if (boton.equals(btJToolBar))
			new E11JToolBar(this);
		else if (boton.equals(btLeerArchivo))
			new E01LeerArchivos(this);
		else if (boton.equals(btEscribirArchivo))
			new E02EscribirArchivos(this);
		else if (boton.equals(btLeerEscribirBytes))
			new E03LeerEscribirBytes(this);
		else if (boton.equals(btSerializacion))
			new E04Serializacion(this);
		else if (boton.equals(btFile))
			new E05File();
		else if (boton.equals(btThreads))
			new E01Threads(this);
		else if (boton.equals(btSincronizacionThreads))
			new E02SincronizarThreads(this);
		else if (boton.equals(btSincronizacionThreadsSincronized))
			new E03SincronizarThreadsConSyncronized(this);
		else if (boton.equals(btCliente))
			new E01Cliente(this);
		else if (boton.equals(btServidor))
			new E01Servidor(this);
		else if (boton.equals(btChatCliente))
			new E03ChatCliente(this);
		else if (boton.equals(btChatServidor))
			new E03ChatServidor(this);
		else if (boton.equals(btSalir))
			System.exit(0);
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.exit(0);
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}