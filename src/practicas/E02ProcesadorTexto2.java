package practicas;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.StyledEditorKit.ForegroundAction;

@SuppressWarnings("serial")
public class E02ProcesadorTexto2 extends JDialog implements CaretListener, FocusListener {

	private JPanel panel;
	private JTextPane hoja;
	private JMenuBar menuBar;
	private JPopupMenu emergente;
	private String fuente;
	private int tamano, estilo;
	private ButtonGroup g1, g2;
	private JToolBar toolBar;
	private JToggleButton btNegrita, btCursiva, btSubrayado, btRojo, btAzul, btVerde, btLeft, btCenter, btRight,
			btJustified;

	public E02ProcesadorTexto2(JFrame f_ppal) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		setTitle("Video 106	y 112- Procesador de Texto Mejorado.");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);

		setSize(500, 400);
		setLocationRelativeTo(f_ppal);

		// asigna el foco al JTextPane
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowOpened(WindowEvent e) {
				hoja.requestFocusInWindow();
			}
		});
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	public void cargarComponentes() {
		// generar menu
		menuBar = new JMenuBar();
		g1 = new ButtonGroup();
		g2 = new ButtonGroup();

		crearMenu("Fuente");
		crearSubMenu(menuBar.getMenu(0), "Arial", "Tipo de letra en Arial", 'A', 1, g1);
		crearSubMenu(menuBar.getMenu(0), "Times New Roman", "Tipo de letra en Courier", 'T', 1, g1);
		crearSubMenu(menuBar.getMenu(0), "Verdana", "Tipo de letra en Verdana", 'V', 1, g1);
		// listeners de familia
		menuBar.getMenu(0).getItem(0).addActionListener(
				new StyledEditorKit.FontFamilyAction("cambia_familia", menuBar.getMenu(0).getItem(0).getText()));
		menuBar.getMenu(0).getItem(1).addActionListener(
				new StyledEditorKit.FontFamilyAction("cambia_familia", menuBar.getMenu(0).getItem(1).getText()));
		menuBar.getMenu(0).getItem(2).addActionListener(
				new StyledEditorKit.FontFamilyAction("cambia_familia", menuBar.getMenu(0).getItem(2).getText()));
		menuBar.getMenu(0).getItem(0).setSelected(true);

		crearMenu("Estilo");
		crearSubMenu(menuBar.getMenu(1), "Negrita", "Estilo en negrita", 'N', 0, null);
		crearSubMenu(menuBar.getMenu(1), "Cursiva", "Estilo en cursiva", 'K', 0, null);
		// listeners de estilo
		menuBar.getMenu(1).getItem(0).addActionListener(new StyledEditorKit.BoldAction());
		menuBar.getMenu(1).getItem(1).addActionListener(new StyledEditorKit.ItalicAction());

		crearMenu("Tamaño");
		crearSubMenu(menuBar.getMenu(2), "12", "Tamaño de letra 12", ' ', 1, g2);
		crearSubMenu(menuBar.getMenu(2), "16", "Tamaño de letra 16", ' ', 1, g2);
		crearSubMenu(menuBar.getMenu(2), "20", "Tamaño de letra 20", ' ', 1, g2);
		crearSubMenu(menuBar.getMenu(2), "24", "Tamaño de letra 24", ' ', 1, g2);
		// listeners de tamaño
		menuBar.getMenu(2).getItem(0).addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño",
				Integer.parseInt(menuBar.getMenu(2).getItem(0).getText())));
		menuBar.getMenu(2).getItem(1).addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño",
				Integer.parseInt(menuBar.getMenu(2).getItem(1).getText())));
		menuBar.getMenu(2).getItem(2).addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño",
				Integer.parseInt(menuBar.getMenu(2).getItem(2).getText())));
		menuBar.getMenu(2).getItem(3).addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño",
				Integer.parseInt(menuBar.getMenu(2).getItem(3).getText())));
		menuBar.getMenu(2).getItem(0).setSelected(true);

		// distribuir objetos en el Dialog
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;// alto
		gbc.gridwidth = 1;// ancho
		gbc.insets = new Insets(1, 0, 0, 0);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		panel.add(menuBar, gbc);

		toolBar = new JToolBar();
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		toolBar.setFloatable(false);
		cargarToolBar();
		panel.add(toolBar, gbc);

		hoja = new JTextPane();
		hoja.setAutoscrolls(true);
		JScrollPane scroll = new JScrollPane(hoja);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weighty = 1.0;
		gbc.insets = new Insets(15, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		panel.add(scroll, gbc);
		btCursiva.requestFocusInWindow();

		fuente = "Arial";
		tamano = 12;
		estilo = Font.PLAIN;

		emergente = new JPopupMenu("Estilo");
		crearSubMenu(emergente, "Negrita", "Estilo en negrita", ' ', -1, null);
		crearSubMenu(emergente, "Cursiva", "Estilo en cursiva", ' ', -1, null);
		hoja.setComponentPopupMenu(emergente);
		JMenuItem negrita = (JMenuItem) emergente.getComponent(0);
		JMenuItem cursiva = (JMenuItem) emergente.getComponent(1);
		negrita.addActionListener(new StyledEditorKit.BoldAction());
		cursiva.addActionListener(new StyledEditorKit.ItalicAction());

		hoja.setFont(new Font(fuente, estilo, tamano));
		hoja.addCaretListener(this);

		// Lista de fuentes
		// GraphicsEnvironment environment =
		// GraphicsEnvironment.getLocalGraphicsEnvironment();
		// String[] listaFuentes = environment.getAvailableFontFamilyNames();
		// for (int i = 0; i < listaFuentes.length; i++)
		// System.out.println(listaFuentes[i]);
	}

	private void cargarToolBar() {
		
		btNegrita = configuraBoton("src/graficos/bold.png");
		btCursiva = configuraBoton("src/graficos/italic.png");
		btSubrayado = configuraBoton("src/graficos/underline.png");
		toolBar.addSeparator();
		btRojo = configuraBoton("src/graficos/rojo2.png");
		btVerde = configuraBoton("src/graficos/verde2.png");
		btAzul = configuraBoton("src/graficos/azul2.png");
		toolBar.addSeparator();
		btLeft = configuraBoton("src/graficos/left_alignment.png");
		btCenter = configuraBoton("src/graficos/center_alignment.png");
		btRight = configuraBoton("src/graficos/right_alignment.png");
		btJustified = configuraBoton("src/graficos/justified_alignment.png");

		btNegrita.addActionListener(new StyledEditorKit.BoldAction());
		btCursiva.addActionListener(new StyledEditorKit.ItalicAction());
		btSubrayado.addActionListener(new StyledEditorKit.UnderlineAction());
		btRojo.addActionListener(new ForegroundAction("accion_rojo", Color.RED));
		btVerde.addActionListener(new ForegroundAction("accion_verde", Color.GREEN));
		btAzul.addActionListener(new ForegroundAction("accion_azul", Color.BLUE));
		btLeft.addActionListener(new StyledEditorKit.AlignmentAction("left", StyleConstants.ALIGN_LEFT));
		btCenter.addActionListener(new StyledEditorKit.AlignmentAction("center", StyleConstants.ALIGN_CENTER));
		btRight.addActionListener(new StyledEditorKit.AlignmentAction("right", StyleConstants.ALIGN_RIGHT));
		btJustified.addActionListener(new StyledEditorKit.AlignmentAction("right", StyleConstants.ALIGN_JUSTIFIED));

		btLeft.setSelected(true);
	}

	private JToggleButton configuraBoton(String ruta) {
		ImageIcon icono = new ImageIcon(new ImageIcon(ruta).getImage().getScaledInstance(12, -1, Image.SCALE_DEFAULT));
		JToggleButton boton = new JToggleButton(icono);
		boton.addFocusListener(this);
		toolBar.add(boton);
		return boton;
	}

	private void crearMenu(String titulo) {
		JMenu menu = new JMenu(titulo);
		menuBar.add(menu);
	}

	private void crearSubMenu(JComponent padre, String titulo, String desc, char atajo, int tipoSubMenu,
			ButtonGroup grupo) {
		JMenuItem item;
		if (tipoSubMenu == 0)
			item = new JCheckBoxMenuItem(titulo);
		else if (tipoSubMenu == 1) {
			item = new JRadioButtonMenuItem(titulo);
			grupo.add(item);
		} else {
			item = new JMenuItem(titulo);
		}

		item.setToolTipText(desc);

		if (atajo != ' ') {
			item.setAccelerator(KeyStroke.getKeyStroke(atajo, InputEvent.ALT_DOWN_MASK));
		}

		if (padre != null)
			padre.add(item);
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		AttributeSet atributos = hoja.getCharacterAttributes();
		AttributeSet atributos2 = hoja.getInputAttributes();// retorna el atributo de entrada

		// controlar fuente
		String fontFamily = atributos == null ? null : (String) atributos.getAttribute(StyleConstants.FontFamily);

		for (int i = 0; i < menuBar.getMenu(0).getItemCount(); i++)
			if (menuBar.getMenu(0).getItem(i).getText().equals(fontFamily))
				menuBar.getMenu(0).getItem(i).setSelected(true);

		// controlar estilo
		boolean bold = StyleConstants.isBold(atributos);
		boolean italic = StyleConstants.isItalic(atributos);
		boolean subrayada = StyleConstants.isUnderline(atributos);
		menuBar.getMenu(1).getItem(0).setSelected(bold);
		menuBar.getMenu(1).getItem(1).setSelected(italic);
		btNegrita.setSelected(bold);
		btCursiva.setSelected(italic);
		btSubrayado.setSelected(subrayada);

		// controla color de fuente
		boolean rojo = StyleConstants.getForeground(atributos2) == Color.RED ? true : false;
		boolean verde = StyleConstants.getForeground(atributos2) == Color.GREEN ? true : false;
		boolean azul = StyleConstants.getForeground(atributos2) == Color.BLUE ? true : false;
		btRojo.setSelected(rojo);
		btVerde.setSelected(verde);
		btAzul.setSelected(azul);

		// controla alineacion
		boolean left = StyleConstants.getAlignment(atributos) == StyleConstants.ALIGN_LEFT ? true : false;
		boolean center = StyleConstants.getAlignment(atributos) == StyleConstants.ALIGN_CENTER ? true : false;
		boolean right = StyleConstants.getAlignment(atributos) == StyleConstants.ALIGN_RIGHT ? true : false;
		boolean justified = StyleConstants.getAlignment(atributos) == StyleConstants.ALIGN_JUSTIFIED ? true : false;
		btLeft.setSelected(left);
		btCenter.setSelected(center);
		btRight.setSelected(right);
		btJustified.setSelected(justified);

		// controla tamaño
		String fontSize = atributos == null ? null : atributos.getAttribute(StyleConstants.FontSize).toString();
		for (int i = 0; i < menuBar.getMenu(2).getItemCount(); i++)
			if (menuBar.getMenu(2).getItem(i).getText().equals(fontSize))
				menuBar.getMenu(2).getItem(i).setSelected(true);
	}

	@Override
	public void focusGained(FocusEvent e) {
		hoja.requestFocusInWindow();
		hoja.setSelectionStart(hoja.getSelectionStart());


		JToggleButton button = (JToggleButton) e.getSource();
		if (button.equals(btLeft)) {
			btCenter.setSelected(false);
			btRight.setSelected(false);
			btJustified.setSelected(false);
		} else if (button.equals(btCenter)) {
			btLeft.setSelected(false);
			btRight.setSelected(false);
			btJustified.setSelected(false);
		} else if (button.equals(btRight)) {
			btLeft.setSelected(false);
			btCenter.setSelected(false);
			btJustified.setSelected(false);
		} else if (button.equals(btJustified)) {
			btLeft.setSelected(false);
			btCenter.setSelected(false);
			btRight.setSelected(false);
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		hoja.setCaretPosition(hoja.getSelectionEnd());
	}
}
