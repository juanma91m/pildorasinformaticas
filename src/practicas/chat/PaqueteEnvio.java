package practicas.chat;

import java.io.Serializable;

public class PaqueteEnvio implements Serializable{
	private static final long serialVersionUID = 1L;
	private String mensaje,nick;

	public PaqueteEnvio(String mensaje, String nick) {
		this.mensaje = mensaje;
		this.nick = nick;
	}
	
	public PaqueteEnvio() {
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
}