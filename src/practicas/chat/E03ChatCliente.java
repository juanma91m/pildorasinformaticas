package practicas.chat;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class E03ChatCliente extends JDialog implements ActionListener, WindowListener {

	private static final long serialVersionUID = 1L;

	private JTextArea taMensaje, taChat;
	private JTextField tfNick;
	private JButton btEnviar, btConectar;
	private boolean conectado = false;
	private JPanel panel;
	private String ipServidor = "localhost";

	public E03ChatCliente(JFrame f_ppal) {
		super(f_ppal, ModalityType.MODELESS);
		setTitle("Video 192 - Sockets chat (Cliente)");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);

		setSize(500, 500);

		setLocation(100, 200);
		
		addWindowListener(this);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weighty = 0.0;
		gbc.weightx = 1.0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(10, 10, 10, 10);

		JLabel label = new JLabel("CHAT", SwingConstants.CENTER);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		panel.add(label, gbc);

		label = new JLabel("Nick:");
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weightx = 0.0;
		panel.add(label, gbc);

		tfNick = new JTextField();
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.5;
		panel.add(tfNick, gbc);

		btConectar = new JButton("Conectar");
		gbc.gridy = 1;
		gbc.gridx = 2;
		gbc.weighty = 0.0;
		gbc.weightx = 0.0;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(btConectar, gbc);
		btConectar.setPreferredSize(new Dimension(123, 25));

		taChat = new JTextArea();
		JScrollPane scroll = new JScrollPane(taChat);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 3;
		gbc.weightx = 1.0;
		gbc.weighty = 0.8;
		gbc.fill = GridBagConstraints.BOTH;
		panel.add(scroll, gbc);
		taChat.setEditable(false);

		gbc.gridy = 3;
		gbc.gridx = 0;
		gbc.weighty = 0.0;
		gbc.insets = new Insets(0, 0, 0, 0);
		panel.add(new JSeparator(SwingConstants.HORIZONTAL), gbc);
		gbc.insets = new Insets(10, 10, 10, 10);

		taMensaje = new JTextArea();
		JScrollPane scroll2 = new JScrollPane(taMensaje);
		gbc.gridy = 4;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		gbc.weighty = 0.2;
		gbc.fill = GridBagConstraints.BOTH;
		panel.add(scroll2, gbc);

		btEnviar = new JButton("Enviar");
		gbc.gridy = 4;
		gbc.gridx = 2;
		gbc.gridwidth = 1;
		gbc.weighty = 0.0;
		gbc.weightx = 0.0;
		gbc.fill = GridBagConstraints.BOTH;
		panel.add(btEnviar, gbc);

		btEnviar.addActionListener(this);
		btConectar.addActionListener(this);
	}

	private boolean conectar() {
		if (tfNick.getText() != null && !tfNick.getText().equals("")) {
			try {
				Socket socket = new Socket(ipServidor, 9189);
				DataInputStream retorno = new DataInputStream(socket.getInputStream());
				if (retorno.readBoolean()) {
					DataOutputStream conexion = new DataOutputStream(socket.getOutputStream());
					conexion.writeUTF(tfNick.getText());
					conectado = true;
					tfNick.setEditable(false);
					btConectar.setText("Desconectar");
					conexion.close();

				} else
					System.err.println("Servidor apagado o inaccesible.");
				retorno.close();

				socket.close();
			} catch (IOException e) {
				System.err.println("Servidor apagado o inaccesible.");
			}
		} else
			System.err.println("El nick está vacío.");
		return conectado;
	}

	private boolean desconectar() {
		try {
			Socket socket = new Socket(ipServidor, 9188);
			DataOutputStream conexion = new DataOutputStream(socket.getOutputStream());
			DataInputStream retorno = new DataInputStream(socket.getInputStream());
			conexion.writeUTF(tfNick.getText());

			if (retorno.readBoolean()) {
				conectado = false;
				btConectar.setText("Conectar");
				tfNick.setEditable(true);
			} else
				System.err.println("Servidor apagado o inaccesible.");

			conexion.close();
			socket.close();
		} catch (IOException e) {
			System.err.println("Servidor apagado o inaccesible.");
		}
		return conectado;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(btEnviar)) {
			if (conectado) {
				try {
					Socket conector = new Socket(ipServidor, 9179);
					PaqueteEnvio datos = new PaqueteEnvio(taMensaje.getText(), tfNick.getText());
					ObjectOutputStream salida = new ObjectOutputStream(conector.getOutputStream());
					salida.writeObject(datos);

					salida.close();
					conector.close();
				} catch (IOException er) {
					System.err.println("Servidor apagado o inaccesible.");
				}
			} else {
				System.err.println(
						"El cliente no se encuentra conectado al servidor. Elegir un nick y presionar en \"Conectar\"");
			}
		} else if (boton.equals(btConectar)) {
			if (conectado)
				desconectar();
			else
				conectar();
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {
		if (conectado)
			desconectar();
	}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowDeactivated(WindowEvent e) {}
}