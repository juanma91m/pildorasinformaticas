package practicas.chat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class E03ChatServidor extends JDialog implements ActionListener, WindowListener {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTextArea taReceptor;
	private JList<String> listaConectados;
	private JButton btOnOff;
	private ImageIcon iconoOn, iconoOff;
	private boolean encendido = false;
	private DefaultListModel<String> dlm;
	private OyenteConexiones oc;
	private OyenteDesconexiones od;
	private OyenteChat och;
	private Thread oyenteConexiones, oyenteDesconexiones, oyenteChat;

	public E03ChatServidor(JFrame f_ppal) {
		super(f_ppal, ModalityType.MODELESS);
		setTitle("Video 192 - Sockets chat (Servidor)");
		setBackground(SystemColor.window);

		panel = new JPanel();
		cargarComponentes();
		add(panel);

		setSize(500, 500);
		setLocation(700, 200);

		addWindowListener(this);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void cargarComponentes() {
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(10, 10, 10, 10);

		taReceptor = new JTextArea();
		taReceptor.setEditable(false);
		JScrollPane scroll = new JScrollPane(taReceptor);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		panel.add(scroll, gbc);

		dlm = new DefaultListModel<String>();
		listaConectados = new JList<String>(dlm);
		JScrollPane scroll2 = new JScrollPane(listaConectados);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		panel.add(scroll2, gbc);

		ImageIcon icono = new ImageIcon("src/graficos/rojo2.png");
		iconoOff = new ImageIcon(icono.getImage().getScaledInstance(20, -1, Image.SCALE_DEFAULT));
		icono = new ImageIcon("src/graficos/verde2.png");
		iconoOn = new ImageIcon(icono.getImage().getScaledInstance(20, -1, Image.SCALE_DEFAULT));

		btOnOff = new JButton("Apagado", iconoOff);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(btOnOff, gbc);

		btOnOff.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(btOnOff)) {
			if (encendido) {
				btOnOff.setText("Apagado");
				btOnOff.setIcon(iconoOff);
				encendido = false;

				for (Thread t : Thread.getAllStackTraces().keySet())
					if (t.getName().contains("Thread-") && (!t.getName().equals("Thread-0") && !t.isInterrupted()
							&& t.getState() != Thread.State.TERMINATED)) {
						t.interrupt();
						t = null;
					}

				try {
					och.getOyenteChat().close();
					oc.getOyenteConexiones().close();
					od.getOyenteDesconexiones().close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			} else {
				btOnOff.setText("Encendido");
				btOnOff.setIcon(iconoOn);
				encendido = true;

				oc = new OyenteConexiones();
				od = new OyenteDesconexiones();
				och = new OyenteChat();

				oyenteConexiones = new Thread(oc);
				oyenteDesconexiones = new Thread(od);
				oyenteChat = new Thread(och);

				oyenteChat.start();
				oyenteConexiones.start();
				oyenteDesconexiones.start();
			}
		}
	}

	class OyenteConexiones implements Runnable {

		private ServerSocket oyenteConexiones;

		@Override
		public void run() {
			try {
				oyenteConexiones = new ServerSocket(9189);

				while (true) {
					Socket receptor = oyenteConexiones.accept();
					DataOutputStream retorno = new DataOutputStream(receptor.getOutputStream());
					if (encendido) {
						DataInputStream entrada = new DataInputStream(receptor.getInputStream());
						retorno.writeBoolean(true);
						String nick = entrada.readUTF();
						dlm.addElement(nick);
					} else {
						retorno.writeBoolean(false);
					}
					receptor.close();
				}
			} catch (IOException e) {
					System.err.println(e.getMessage());
			}
		}

		public ServerSocket getOyenteConexiones() {
			return oyenteConexiones;
		}
	}

	class OyenteDesconexiones implements Runnable {

		private ServerSocket oyenteDesconexiones;

		@Override
		public void run() {
			try {
				oyenteDesconexiones = new ServerSocket(9188);

				while (true) {
					Socket receptor = oyenteDesconexiones.accept();
					DataOutputStream retorno = new DataOutputStream(receptor.getOutputStream());
					if (encendido) {
						retorno.writeBoolean(true);
						DataInputStream entrada = new DataInputStream(receptor.getInputStream());
						String nick = entrada.readUTF();
						dlm.removeElement(nick);
					} else {
						retorno.writeBoolean(false);
					}
					receptor.close();
				}
			} catch (IOException e) {
					System.err.println(e.getMessage());
			}
		}

		public ServerSocket getOyenteDesconexiones() {
			return oyenteDesconexiones;
		}

		public void setOyenteDesconexiones(ServerSocket oyenteDesconexiones) {
			this.oyenteDesconexiones = oyenteDesconexiones;
		}
	}

	class OyenteChat implements Runnable {

		private ServerSocket oyenteChat;

		@Override
		public void run() {
			try {
				oyenteChat = new ServerSocket(9179);

				while (true) {
					Socket receptor = oyenteChat.accept();
					if (encendido) {
						ObjectInputStream entrada = new ObjectInputStream(receptor.getInputStream());

						PaqueteEnvio datos = (PaqueteEnvio) entrada.readObject();
						taReceptor.append(datos.getMensaje() + "\n");
					}
					receptor.close();
				}
			} catch (IOException e) {
					System.err.println(e.getMessage());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		public ServerSocket getOyenteChat() {
			return oyenteChat;
		}

		public void setOyenteChat(ServerSocket oyenteChat) {
			this.oyenteChat = oyenteChat;
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
		for (Thread t : Thread.getAllStackTraces().keySet())
			if (t.getName().contains("Thread-") && (!t.getName().equals("Thread-0") && !t.isInterrupted()
					&& t.getState() != Thread.State.TERMINATED)) {
				t.interrupt();
				t = null;
			}

		try {
			och.getOyenteChat().close();
			oc.getOyenteConexiones().close();
			od.getOyenteDesconexiones().close();
		} catch (IOException e1) {
		}
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}